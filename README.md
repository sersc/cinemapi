# ShowTracker #

This project uses a custom API to create/delete and display movies.

### Getting Started ###

* Launch Spring app with: `./gradlew bootRun` or `grandlew bootRun` on **windows**
* Launch Django app with: python manage.py runserver.
* Launch XAMPP (MySQL mandatory)
* Go to `http://127.0.0.1/` and the Web application will show

### Built With ###

* Spring
* Django 1.11.8
* MySQL

### Authors ###

* Sergio Soler