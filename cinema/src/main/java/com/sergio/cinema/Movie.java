package com.sergio.cinema;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by DAW on 19/02/2018.
 */

@Entity
@Table(name = "movie")
public class Movie {

    @Id
    @GeneratedValue
    private int id;
    @Column
    private String name;
    @Column
    private String genre;
    @Column
    private String synopsis;
    @Column
    private String director;
    @Column
    private Date aired;
    @Column
    private int duration;
    @Column
    private float price;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGenre() {
        return genre;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public String getDirector() {
        return director;
    }

    public Date getAired() {
        return aired;
    }

    public int getDuration() {
        return duration;
    }

    public float getPrice() {
        return price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setAired(Date aired) {
        this.aired = aired;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}



