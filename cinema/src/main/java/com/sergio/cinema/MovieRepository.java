package com.sergio.cinema;

import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by DAW on 19/02/2018.
 */
public interface MovieRepository extends CrudRepository<Movie, Integer> {

    List<Movie> findAll();
    List<Movie> findByNameAndAiredAfter(String name, Date date);

}