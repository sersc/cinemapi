package com.sergio.cinema;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * Created by DAW on 19/02/2018.
 */
@RestController
public class MovieController {

    @Autowired
    private MovieRepository repository;

    /**
     * Obtiene todas las opiniones de los usuarios
     *
     * @return
     */
    @RequestMapping("/movies")
    public List<Movie> getMovies() {

        List<Movie> movieList = repository.findAll();
        return movieList;
    }

    @RequestMapping("/movies_after")
    public List<Movie> getMoviesAfter(String name, Date date) {

        List<Movie> movieList = repository.findByNameAndAiredAfter(name, date);
        return movieList;
    }
}