from django.shortcuts import render, redirect
from .models import Movie
from .serializer import MovieSerializer
from .forms import MovieForm
import requests
import datetime
from django.contrib import messages


def get_movies(request):
    ws = requests.get('http://localhost:8082/all_movies')
    json = ws.json()
    for i in range(0, len(json)):
        json[i]["aired"] = datetime.datetime.fromtimestamp(json[i]["aired"] / 1000).strftime('%Y-%m-%d')
    serializer = MovieSerializer(data=json, many=True)
    if serializer.is_valid():
        movie_list = serializer.data
        context = {'movie_list': movie_list}
        return render(request, 'cinema/index.html', context)
    else:
        print(serializer.errors)

    return render(request, 'cinema/index.html')


def get_tabla(request):
    ws = requests.get('http://localhost:8082/all_movies')
    json = ws.json()
    serializer = MovieSerializer(data=json, many=True)
    for i in range(0, len(json)):
        json[i]["aired"] = datetime.datetime.fromtimestamp(json[i]["aired"] / 1000).strftime('%Y-%m-%d')
        print(json[i]["id"])
    if serializer.is_valid():
        movie_list = serializer.data
        print(movie_list)
        context = {'movie_list': movie_list}
        return render(request, 'cinema/tablas.html', context)
    else:
        print(serializer)

    return render(request, 'cinema/tablas.html')


def new_movie(request):
    return render(request, 'cinema/new_movie.html')


def create_movie(request):
    if request.method == 'POST':
        form = MovieForm(request.POST, request.FILES)
        if form.is_valid():
            url = 'http://localhost:8082/add_movie?name=' + form.cleaned_data['name'] +\
                  '&genre=' + form.cleaned_data['genre'] +\
                  '&director=' + form.cleaned_data['director'] + \
                  '&synopsis=' + form.cleaned_data['synopsis'] + \
                  '&aired=' + str(form.cleaned_data['aired']) +\
                  '&duration=' + str(form.cleaned_data['duration']) +\
                  '&price=' + str(form.cleaned_data['price'])
            ws = requests.get(url)

            messages.success(request, 'Show created successfully')
            return redirect('tablas')
        else:
            return render(request, 'cinema/new_movie.html', {'form': form})


def delete_movie(request, id_movie):
    requests.get('http://localhost:8082/delete_movie?id=' + id_movie)
    return render(request, 'cinema/tablas.html')
