from django.db import models


class Movie(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    genre = models.CharField(max_length=200)
    duration = models.IntegerField(default=0)
    synopsis = models.CharField(max_length=200)
    director = models.CharField(max_length=200)
    aired = models.DateField(blank=True, null=True)
    price = models.FloatField(default=0.0)
