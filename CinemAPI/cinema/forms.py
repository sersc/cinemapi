from django import forms
from .models import Movie


class MovieForm(forms.ModelForm):
    class Meta:
        fields = ('name', 'genre', 'director', 'synopsis', 'aired', 'duration', 'price')
        model = Movie
