# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-02-20 15:28
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cinema', '0004_auto_20180220_1626'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movie',
            name='aired',
            field=models.DateField(default=datetime.date(2018, 2, 20), null=True),
        ),
    ]
