from rest_framework import serializers
from .models import Movie


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'name', 'genre', 'duration', 'synopsis', 'director', 'aired', 'price')
        model = Movie
