from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.get_movies, name='index'),
    url(r'^tablas', views.get_tabla, name='tablas'),
    url(r'^new_movie', views.new_movie, name='new_movie'),
    url(r'^create_movie', views.create_movie, name='create_movie'),
    url(r'^delete_movie/(?P<id_movie>[0-9]+)/$', views.delete_movie, name='delete_movie'),
]
